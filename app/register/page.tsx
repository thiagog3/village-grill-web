// import { useState } from "react";

import { Card, CardContent, CardHeader, CardTitle, CardFooter, CardDescription } from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";

import { Key } from "lucide-react"

import { register } from './actions';

async function getApartmentByHashId(hashId: string) {
  const res = await fetch(`https://village-grill-api.onrender.com/apartment/${hashId}`)

  if (!res.ok) {
    throw new Error('Failed to fetch data')
  }
  return res.json()
}

export default async function Register({
  params,
  searchParams,
}: {
  params: { slug: string }
  searchParams: { [key: string]: string | string[] | undefined }
}) {
  // const [password, setPassword] = useState("");
  // const [confirmPassword, setConfirmPassword] = useState("");
  try{
    const apartment = await getApartmentByHashId(searchParams['hash'] as string);
    return (
      <main className="flex min-h-screen flex-col justify-center items-center p-8">
        <div className="flex flex-col items-center justify-center w-full">
          <div className="space-y-2">
            <Card className="w-[350px] text-center">
            <form action={register}>
              <CardHeader>
                <CardTitle>Apartamento {apartment?.number}</CardTitle>
                <CardDescription>Escolha uma senha de acesso</CardDescription>
              </CardHeader>
              <CardContent>
                <input type="hidden" name="hash" value={searchParams['hash']} />
                <div className="space-y-3">
                  <Input
                    type="password"
                    name="password"
                    placeholder="Senha"
                  />
                  <Input
                    type="password"
                    name="confirm-password"
                    placeholder="Confirmar senha"
                  />
                </div>
              </CardContent>
              <CardFooter>
                <Button
                  className="w-full"
                  type="submit"
                ><Key className="mr-2 h-4 w-4" /> Definir senha</Button>
              </CardFooter>
              </form>
            </Card>
          </div>
        </div>
      </main>
    );
  } catch (e) {
    return (
      <main className="flex min-h-screen flex-col justify-center items-center p-8">
        <div className="flex flex-col items-center justify-center w-full">
          <div className="space-y-2">
            <Card className="w-[350px] text-center">
              <CardHeader>
                <CardTitle>Acesso inexistente</CardTitle>
              </CardHeader>
              <CardContent>
                O link utilizado é inválido ou a senha já foi definida. Caso não tenha definido a senha, entre em contato com o síndico.
              </CardContent>
            </Card>
          </div>
        </div>
      </main>
    );
  }
}