'use server'

import { redirect } from 'next/navigation'

export async function register(formData: FormData) {
  const response = await fetch("https://village-grill-api.onrender.com/apartment/set-password", {
    method: "PUT",
    cache: 'no-store',
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      hash: formData.get('hash') as string,
      password: formData.get('password') as string,
    }),
  });
  await response.json();

  if (!response.ok){
      throw new Error('Error setting password');
  }
  redirect('/');
}
