"use client"

import { useState, useTransition } from "react";
import Image from 'next/image';

import { Card, CardContent, CardHeader, CardTitle, CardFooter } from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { useToast } from "@/components/ui/use-toast"
import { ScrollArea } from "@/components/ui/scroll-area"

import { cn } from "@/lib/utils"

import {
  Command,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
} from "@/components/ui/command"

import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover"

import { Key, Check, ChevronsUpDown } from "lucide-react"

import { doLogin } from './actions';

const apartments: string[] = [];

for (let i = 1; i <= 15; i++) {
  for (let k = 1; k <= 4; k++) {
    const number = `${i}${k}`;
    apartments.push(number);
  }
}

export default function Login() {
  const [password, setPassword] = useState("");
  const [open, setOpen] = useState(false)
  const [number, setNumber] = useState("")

  const { toast } = useToast();
  const [, startTransition] = useTransition()

  const handleLogin = () => {
    startTransition(async () => {
      try{
        await doLogin({number, password})
      } catch (error) {
        toast({
          variant: 'destructive',
          title: "Login inválido",
          description: "Verifique o número do apartamento e a senha.",
        });
      }
    });
  }


  return (
    <main className="flex min-h-screen flex-col justify-center items-center p-8">
      <div className="flex flex-col items-center justify-center w-full">
        <div className="mb-4">
          <Image src="/logo.svg" alt="my image" width={200} height={45} />
        </div>
        <div className="space-y-2">
          <Card className="w-[350px] text-center">
            <CardHeader>
              <CardTitle>Acessar</CardTitle>
            </CardHeader>
            <CardContent>
              <form onSubmit={handleLogin}>
                <div className="space-y-3">
                <Popover open={open} onOpenChange={setOpen} modal={true}>
                  <PopoverTrigger asChild>
                    <Button
                      variant="outline"
                      role="combobox"
                      aria-expanded={open}
                      className="w-full justify-between"
                    >
                      {number
                        ? apartments.find((framework) => framework === number)
                        : "Selecione o apartamento..."}
                      <ChevronsUpDown className="ml-2 h-4 w-4 shrink-0 opacity-50" />
                    </Button>
                  </PopoverTrigger>
                  <PopoverContent className="p-0">
                      <Command>
                        <CommandInput placeholder="Buscar apartamento..." />
                        <CommandEmpty>Apartamento não existe.</CommandEmpty>
                        <ScrollArea className="flex max-h-[200px] flex-col" type="always">
                          <CommandGroup>
                            {apartments.map((framework) => (
                              <CommandItem
                                key={framework}
                                onSelect={(currentValue) => {
                                  setNumber(currentValue === number ? "" : currentValue)
                                  setOpen(false)
                                }}
                              >
                                <Check
                                  className={cn(
                                    "mr-2 h-4 w-4",
                                    number === framework ? "opacity-100" : "opacity-0"
                                  )}
                                />
                                {framework}
                              </CommandItem>
                            ))}
                          </CommandGroup>
                        </ScrollArea>
                      </Command>
                  </PopoverContent>
                </Popover>
                <Input
                  type="password"
                  placeholder="Senha"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                </div>
              </form>
            </CardContent>
            <CardFooter>
              <Button
                className="w-full"
                type="button"
                onClick={handleLogin}
              ><Key className="mr-2 h-4 w-4" /> Entrar</Button>
            </CardFooter>
          </Card>
        </div>
      </div>
    </main>
  );
}