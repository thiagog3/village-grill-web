'use server'

import { cookies } from 'next/headers'
import { redirect } from 'next/navigation'

export async function doLogin({ number, password }: { number: string, password: string }) {
  try {
    const response = await fetch("https://village-grill-api.onrender.com/apartment/login", {
      method: "POST",
      cache: 'no-store',
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        number,
        password,
      }),
    });

    const data = await response.json();

    if (response.ok && data.token) {
      const token = data.token;
      cookies().set('token', token);
    } else {
      throw new Error(data.error);
    }
  } catch (error) {
    throw error;
  }
  redirect('/');
}
