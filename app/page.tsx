"use client"

import { useEffect, useState } from "react"

import { useRouter } from 'next/navigation'

import { ReloadIcon } from "@radix-ui/react-icons"

import { Switch } from "@/components/ui/switch"

import { Skeleton } from "@/components/ui/skeleton"

import { ScrollArea } from "@/components/ui/scroll-area"

import { LogOut, AlertCircle } from "lucide-react"

import { parseCookies, destroyCookie } from "nookies";

import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"

import {
  Tabs,
  TabsContent,
  TabsList,
  TabsTrigger,
} from "@/components/ui/tabs"

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table"

import { Button } from "@/components/ui/button"

import {
  Alert,
  AlertDescription,
  AlertTitle,
} from "@/components/ui/alert"

import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog"

 
type Grill = {
  block: string;
  status: 'on' | 'off';
};

type Apartment = {
  number: string;
  block: string;
  status: 'on' | 'off';
}

type Equipment = {
  status: 'online' | 'offline';
}

export default function Home() {
  const [grills, setGrills] = useState<Grill[]>([]);
  const [apartments, setApartments] = useState<Apartment[]>([]);
  const [myApartment, setMyApartment] = useState<Apartment | null>(null);
  const [equipment, setEquipment] = useState<Equipment | null>(null);

  const [isGrillActive, setIsGrillActive] = useState(false);

  const { replace } = useRouter();

  const cookies = parseCookies();
  const token = cookies.token;

  const fetchData = async () => {
    const results = await Promise.all([
      getApartmentData(token),
      getGrillData(),
      getMyApartmentData(token),
      getEquipmentStatus(token),
    ]);

    setApartments(results[0].sort((a: Apartment, b: Apartment) => Number(a.number) > Number(b.number) ? 1 : -1));
    setGrills(results[1].sort((a: Grill, b: Grill) => Number(a.block) > Number(b.block) ? 1 : -1));
    setMyApartment(results[2]);
    setEquipment(results[3]);
  };

  useEffect(() => {
    setTimeout(() => {
      window.scroll(0, 0);
    }, 50);
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      fetchData();
    }, 20000);

    fetchData();
    
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    if (myApartment) {
      setIsGrillActive(myApartment.status === 'on');
    }
  }, [myApartment]);

  function logout() {
    destroyCookie(null, 'token');
    replace('/login');
  }

  async function toggleGrill() {
    setIsGrillActive(!isGrillActive);
    await toggleApartment(token);
    fetchData();
  }

  return (
    <main className="flex min-h-screen flex-col justify-between p-8">
      <div className="flex flex-col items-center justify-center w-full">
        {equipment?.status === 'offline' && (
          <div className="space-y-2 mb-2">
            <Alert variant="destructive" className="w-[350px]">
              <AlertCircle className="h-4 w-4" />
              <AlertTitle>Atenção</AlertTitle>
              <AlertDescription>
                Chaveamento está em modo manual. Será necessário ativar ou desligar o exaustor manualmente na portaria.
              </AlertDescription>
            </Alert>
          </div>
        )}
        <div className="space-y-2">
          <Card className="w-[350px]">
            <CardHeader className="flex flex-row justify-between">
              <div>
                <CardTitle>Apartamento {myApartment?.number}</CardTitle>
                <CardDescription>Está usando sua churrasqueira?</CardDescription>
              </div>
              { equipment?.status === 'online' && (
                <Switch 
                  checked={isGrillActive}
                  onCheckedChange={toggleGrill}
                />
              )}
              { equipment?.status === 'offline' && (
                <AlertDialog>
                <AlertDialogTrigger>
                  <Switch 
                    checked={isGrillActive}
                  />
                </AlertDialogTrigger>
                <AlertDialogContent>
                  <AlertDialogHeader>
                    <AlertDialogTitle>Leia com atenção</AlertDialogTitle>
                    <AlertDialogDescription>
                      Por problemas de conexão, o chaveamento dos exaustores está em modo manual. Será necessário sincronizar o estado do aplicativo com a caixa de exaustores na portaria. Ao descer, confirme o status do exaustor no aplicativo antes de ativar/desativar o seu bloco.
                    </AlertDialogDescription>
                  </AlertDialogHeader>
                  <AlertDialogFooter>
                    <AlertDialogCancel>Cancelar</AlertDialogCancel>
                    <AlertDialogAction onClick={toggleGrill}>Concordo</AlertDialogAction>
                  </AlertDialogFooter>
                </AlertDialogContent>
              </AlertDialog>
              )}
            </CardHeader>
          </Card>
          <Tabs defaultValue="account" className="w-[350px]">
            <TabsList className="grid w-full grid-cols-2">
              <TabsTrigger value="account">Resumo</TabsTrigger>
              <TabsTrigger value="password">Por apartamento</TabsTrigger>
            </TabsList>
            <TabsContent value="account">
              <Card className="w-[350px]">
                <CardHeader className="flex flex-row justify-between">
                  <div>
                    <CardTitle>Exaustores</CardTitle>
                    <CardDescription>Como estão os exaustores?</CardDescription>
                  </div>
                  <Button
                    className="mt-0"
                    variant="outline"
                    size="icon"
                    onClick={() => {
                      fetchData();
                    }}
                  >
                    <ReloadIcon className="h-4 w-4" />
                  </Button>
                </CardHeader>
                <CardContent>
                  <Table>
                    <TableHeader>
                      <TableRow>
                        <TableHead>Bloco</TableHead>
                        <TableHead>Status</TableHead>
                      </TableRow>
                    </TableHeader>
                    <TableBody>
                      {grills.map((grill) => (
                        <TableRow key={grill.block}>
                          <TableCell>{grill.block}</TableCell>
                          <TableCell>
                            { grill.status === 'off' && (
                              <div className="bg-red-500 text-white py-1 px-3 rounded-full shadow inline-block ">
                                Desligado
                              </div>
                            )}
                            { grill.status === 'on' && (
                              <div className="bg-green-500 text-white py-1 px-3 rounded-full shadow inline-block w-[100px] text-center">
                                Ligado
                              </div>
                            )}
                          </TableCell>
                        </TableRow>
                      ))}
                      {grills.length === 0 && (
                        [1,2,3,4].map((i) => (
                          <TableRow key={i}>
                            <TableCell>
                              <Skeleton className="h-4 w-[48px]" />
                            </TableCell>
                            <TableCell>
                              <Skeleton className="h-4 w-[120px]" />
                            </TableCell>
                          </TableRow>
                        ))
                      )}
                    </TableBody>
                  </Table>
                </CardContent>
              </Card>
            </TabsContent>
            <TabsContent value="password">
            <Card className="w-[350px]">
                <CardHeader className="flex flex-row justify-between">
                  <CardTitle>Todos apartamentos</CardTitle>
                </CardHeader>
                <CardContent>
                <ScrollArea className="flex max-h-[400px] flex-col" type="always">
                  <Table>
                    <TableHeader>
                      <TableRow>
                        <TableHead>Apartamento</TableHead>
                        <TableHead>Status</TableHead>
                      </TableRow>
                    </TableHeader>
                    <TableBody>
                    
                      {apartments.map((apartment) => (
                        <TableRow key={apartment.number}>
                          <TableCell>{apartment.number}</TableCell>
                          <TableCell>
                            { apartment.status === 'off' && (
                              <div className="bg-red-500 text-white py-1 px-3 rounded-full shadow inline-block ">
                                Desligado
                              </div>
                            )}
                            { apartment.status === 'on' && (
                              <div className="bg-green-500 text-white py-1 px-3 rounded-full shadow inline-block w-[100px] text-center">
                                Ligado
                              </div>
                            )}
                          </TableCell>
                        </TableRow>
                      ))}
                      {grills.length === 0 && (
                        [1,2,3,4].map((i) => (
                          <TableRow key={i}>
                            <TableCell>
                              <Skeleton className="h-4 w-[48px]" />
                            </TableCell>
                            <TableCell>
                              <Skeleton className="h-4 w-[120px]" />
                            </TableCell>
                          </TableRow>
                        ))
                      )}
                    
                    </TableBody>
                  </Table>
                  </ScrollArea>
                </CardContent>
              </Card>
            </TabsContent>
          </Tabs>

          <Button
            className="w-full"
            type="button"
            variant="ghost"
            onClick={logout}
          ><LogOut className="mr-2 h-4 w-4" /> Sair</Button>
        </div>
      </div>
    </main>
  )
}

async function getGrillData() {
  const response = await fetch('https://village-grill-api.onrender.com/grill', { cache: 'no-cache' });
  const data = await response.json();
  return data;
}

async function getEquipmentStatus(token: string) {
  const response = await fetch('https://village-grill-api.onrender.com/equipment/gran-village', {
    headers: {
      Authorization: `Bearer ${token}`,
      "Cache-Control": "no-cache",
    },
  });
  const data = await response.json();
  return data;
}

async function getApartmentData(token: string) {
  const response = await fetch('https://village-grill-api.onrender.com/apartment', {
    headers: {
      Authorization: `Bearer ${token}`,
      "Cache-Control": "no-cache",
    },
  });
  const data = await response.json();
  return data;
}

async function getMyApartmentData(token: string) {
  const response = await fetch('https://village-grill-api.onrender.com/apartment/my-apartment', {
    headers: {
      Authorization: `Bearer ${token}`,
      "Cache-Control": "no-cache",
    },
  });
  const data = await response.json();
  return data;
}

async function toggleApartment(token: string) {
  const response = await fetch('https://village-grill-api.onrender.com/grill/toggle', {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${token}`,
      "Cache-Control": "no-cache",
    },
  });
  const data = await response.json();
  return data;
}
