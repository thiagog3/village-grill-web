import { NextResponse, NextRequest } from 'next/server'
import { cookies } from 'next/headers'
 
// This function can be marked `async` if using `await` inside
export function middleware(request: NextRequest) {
  // Redirect to the login page if the user is not logged in
  const cookieStore = cookies();

  if (cookieStore.get('token')) {
    return NextResponse.next()
  }

  return NextResponse.redirect(new URL('/login', request.url))
}
 
// See "Matching Paths" below to learn more
export const config = {
  matcher: '/',
}